package ro.ubb.bookstore.web.converter;

import org.springframework.stereotype.Component;
import ro.ubb.bookstore.common.model.Book;
import ro.ubb.bookstore.web.dto.BookDto;

@Component
public class BookConverter extends BaseConverter<Book, BookDto> {
    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .author(dto.getAuthor())
                .title(dto.getTitle())
                .price(dto.getPrice())
                .year(dto.getYear())
                .build();
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = BookDto.builder()
                .author(book.getAuthor())
                .title(book.getTitle())
                .price(book.getPrice())
                .year(book.getYear())
                .build();

        dto.setId(book.getId());
        return dto;
    }
}