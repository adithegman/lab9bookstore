package ro.ubb.bookstore.web.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ro.ubb.bookstore.common.service.BookService;
import ro.ubb.bookstore.common.service.ClientService;
import ro.ubb.bookstore.common.service.PurchaseService;
import ro.ubb.bookstore.web.converter.PurchaseConverter;
import ro.ubb.bookstore.web.dto.PurchaseDto;
import ro.ubb.bookstore.web.dto.PurchasesDto;

@RestController
public class PurchaseController {
    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private PurchaseService purchaseService;

    @Autowired
    private BookService bookService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private PurchaseConverter purchaseConverter;

    @RequestMapping(value = "/purchases", method = RequestMethod.GET)
    PurchasesDto getPurchases() {
        log.trace("getPurchases --- called");

        return new PurchasesDto(purchaseConverter.convertModelsToDtos(
                purchaseService.getAllPurchases()
        ));
    }

    @RequestMapping(value = "/purchases", method = RequestMethod.POST)
    PurchaseDto savePurchase(@RequestBody PurchaseDto purchaseDto) {
        log.trace("savePurchase --- called");

        if(clientService.getClient(purchaseDto.getClient()).isEmpty() ||
           bookService.getBook(purchaseDto.getBook()).isEmpty())
        {
            PurchaseDto ret = new PurchaseDto();
            ret.setId((long)-1);

            return ret;
        }

        return purchaseConverter.convertModelToDto(
                purchaseService.savePurchase(purchaseConverter.convertDtoToModel(purchaseDto))
        );
    }
}
