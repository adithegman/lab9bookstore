package ro.ubb.bookstore.common.repository;

import ro.ubb.bookstore.common.model.Client;

public interface ClientRepository extends BookstoreRepository<Client, Long> {
}
