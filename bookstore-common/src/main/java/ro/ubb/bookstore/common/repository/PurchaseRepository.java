package ro.ubb.bookstore.common.repository;

import ro.ubb.bookstore.common.model.Purchase;

public interface PurchaseRepository extends BookstoreRepository<Purchase, Long> {
}
