package ro.ubb.bookstore.common.repository;
import ro.ubb.bookstore.common.model.Book;

public interface BookRepository extends BookstoreRepository<Book, Long> {
}
