package ro.ubb.bookstore.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.common.model.Book;
import ro.ubb.bookstore.common.repository.BookRepository;

import java.util.List;
import java.util.Optional;

@Service
public class BookServiceImpl implements BookService {
    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Book> getAllBooks() {
        log.trace("getAllBooks --- method entered");
        List<Book> result = bookRepository.findAll();
        log.trace("getAllBooks: result={}", result);

        return result;
    }

    @Override
    public Optional<Book> getBook(Long id) {
        return bookRepository.findById(id);
    }

    @Override
    public Book saveBook(Book book) {
        log.trace("saveBook --- method entered");
        Book saved = bookRepository.save(book);
        log.trace("saveBook: saved entity {}", saved);

        return saved;
    }

    @Override
    @Transactional
    public Book updateBook(Long id, Book book) {

        log.trace("updateBook --- method entered");
        Book updated = bookRepository.findById(id).orElse(book);
        updated.setYear(book.getYear());
        updated.setAuthor(book.getAuthor());
        updated.setPrice(book.getPrice());
        updated.setTitle(book.getTitle());
        log.trace("updateBook: updated entity, old: {}; \nnew: {}", book, updated);

        return updated;
    }

    @Override
    public void deleteBook(Long id) {
        log.trace("deleteBook --- method entered");
        Optional<Book> deleted = bookRepository.findById(id);
        bookRepository.deleteById(id);

        if(deleted.isEmpty())
            log.trace("deleteBook: no book deleted");
        else
            log.trace("deleteBook: deleted book: {}", deleted.get());
    }
}
