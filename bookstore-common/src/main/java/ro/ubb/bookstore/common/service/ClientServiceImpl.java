package ro.ubb.bookstore.common.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ro.ubb.bookstore.common.model.Client;
import ro.ubb.bookstore.common.repository.ClientRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients --- Method called");
        List<Client> result = clientRepository.findAll();
        log.trace("getAllClients: result={}", result);

        return result;
    }

    @Override
    public Optional<Client> getClient(Long id) {
        return clientRepository.findById(id);
    }

    @Override
    public Client saveClient(Client client) {
        log.trace("saveClient --- Method called");
        Client result = clientRepository.save(client);
        log.trace("saveClient: saved client {}", result);

        return result;
    }

    @Override
    @Transactional
    public Client updateClient(Long id, Client client) {
        log.trace("updateClient --- Method called");
        Client result = clientRepository.findById(id).orElse(client);
        result.setFirstName(client.getFirstName());
        result.setLastName(client.getLastName());
        result.setAddress(client.getAddress());
        log.trace("updateClient: updated client {}", result);

        return result;
    }

    @Override
    public void deleteClient(Long id) {
        log.trace("deleteClient --- Method called");
        Optional<Client> deleted = clientRepository.findById(id);
        clientRepository.deleteById(id);

        if(deleted.isEmpty())
            log.trace("deleteClient: no book deleted");
        else
            log.trace("deleteClient: deleted client: {}", deleted.get());
    }
}
