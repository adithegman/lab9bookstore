package ro.ubb.bookstore.client.ui;

import org.springframework.web.client.RestTemplate;
import ro.ubb.bookstore.common.model.Book;
import ro.ubb.bookstore.common.model.Client;
import ro.ubb.bookstore.common.model.Purchase;
import ro.ubb.bookstore.web.converter.ClientConverter;
import ro.ubb.bookstore.web.converter.PurchaseConverter;
import ro.ubb.bookstore.web.dto.*;
import ro.ubb.bookstore.web.converter.BookConverter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class InputOutput {
    private final Scanner scanner = new Scanner(System.in);
    private final BookConverter bookConverter = new BookConverter();
    private final ClientConverter clientConverter = new ClientConverter();
    private final PurchaseConverter purchaseConverter = new PurchaseConverter();
    private final RestTemplate restTemplate;
    public final String URL = "http://localhost:8080/api/";
    public final String URL_BOOKS = "http://localhost:8080/api/books";
    public final String URL_CLIENTS = "http://localhost:8080/api/clients";
    public final String URL_PURCHASES = "http://localhost:8080/api/purchases";

    public InputOutput(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }




    // ------------------ BOOK ------------------
    private Book readBook() {
        System.out.println("ID:");
        Long id = scanner.nextLong();
        scanner.nextLine(); // Must be here otherwise Title is not read, reason: nextLong doesn't read new line?

        System.out.println("Title:");
        String title = scanner.nextLine();

        System.out.println("Author:");
        String author = scanner.nextLine();

        System.out.println("Price:");
        int price = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Release Year:");
        int year = scanner.nextInt();

        Book book = new Book(title, author, price, year);
        book.setId(id);
        return book;
    }

    private void showBooks() {
        BooksDto allBooks = restTemplate.getForObject(URL_BOOKS, BooksDto.class);
        if (allBooks != null)
            allBooks.getBooks().forEach(System.out::println);
        else System.out.println("No books to show! Is the server down?");
    }

    private void addBook() {
        Book book = readBook();
        BookDto savedBook = restTemplate.postForObject(URL_BOOKS,
                bookConverter.convertModelToDto(book),
                BookDto.class);

        System.out.println("savedBook: " + savedBook);
    }

    private void updateBook() {
        Book book = readBook();
        restTemplate.put(URL_BOOKS + "/{id}", book, book.getId());
    }

    private void removeBook() {
        System.out.println("remove book with ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();

        restTemplate.delete(URL_BOOKS + "/{id}", id);
    }

    private void bookMenu(){
        while(true){
            System.out.println("1.Add book|2.Show all books|3.Remove book|4.Update book|5.Find by year|6.Find by author|7.Sort Title|0.Go back");
            int x = scanner.nextInt();
            switch(x){
                case 1:
                    addBook();
                    break;
                case 2:
                    showBooks();
                    break;
                case 3:
                    removeBook();
                    break;
                case 4:
                    updateBook();
                    break;
                case 5:
                    //bookYear();
                    break;
                case 6:
                    //bookName();
                    break;
                case 7:
                    /*
                    try{
                        service.sortTitle().forEach(System.out::println);
                    }catch(ValidatorException e){e.printStackTrace();}*/
                case 0:
                    return;
                default:
                    System.out.println("Invalid input");;
            }
        }
    }




    // ------------------ CLIENT ------------------
    private Client readClient(){
        System.out.println("ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        System.out.println("First Name:");
        String first = scanner.nextLine();

        System.out.println("Last Name:");
        String last = scanner.nextLine();

        System.out.println("Address:");
        String addr = scanner.nextLine();

        Client client = new Client(first,last,addr);
        client.setId(id);
        return client;
    }

    private void showClients(){
        ClientsDto allClients = restTemplate.getForObject(URL_CLIENTS, ClientsDto.class);
        if (allClients != null)
            allClients.getClients().forEach(System.out::println);
        else System.out.println("No clients to show! Is the server down?");
    }

    private void addClient() {
        Client client = readClient();
        ClientDto savedClient = restTemplate.postForObject(URL_CLIENTS,
                clientConverter.convertModelToDto(client),
                ClientDto.class);

        System.out.println("savedClient: " + savedClient);
    }

    private void updateClient() {
        Client client = readClient();
        restTemplate.put(URL_CLIENTS + "/{id}", client, client.getId());
    }

    private void removeClient() {
        System.out.println("remove client with ID:");
        Long id = scanner.nextLong();
        scanner.nextLine();

        restTemplate.delete(URL_CLIENTS + "/{id}", id);
    }

    private void clientMenu(){
        while(true){
            System.out.println("1.Add client|2.Show all clients|3.Remove client|4.Update client|5.Find by address|0.Go back");
            int x = scanner.nextInt();
            switch(x){
                case 1:
                    addClient();
                    break;
                case 2:
                    showClients();
                    break;
                case 3:
                    removeClient();
                    break;
                case 4:
                    updateClient();
                    break;
                case 5:
                    //clientAddr();
                    break;
                case 0:
                    return;
                default:
                    System.out.println("Invalid input");;
            }

        }
    }



    // ------------------ PURCHASE ------------------
    private void showPurchases() {
        PurchasesDto allPurchases = restTemplate.getForObject(URL_PURCHASES, PurchasesDto.class);
        if (allPurchases != null)
            allPurchases.getPurchases().forEach(System.out::println);
        else System.out.println("No sale history to show! Is the server down?");
    }

    private void buyBook(){
        System.out.println("Purchase number:");
        Long id = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Client ID:");
        Long cid = scanner.nextLong();
        scanner.nextLine();
        System.out.println("Book ID:");
        Long bid = scanner.nextLong();
        scanner.nextLine();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String transactionDate = dateFormat.format(date);

        Purchase purchase = new Purchase(cid, bid, transactionDate);
        purchase.setId(id);

        PurchaseDto savedPurchase = restTemplate.postForObject(URL_PURCHASES,
                purchaseConverter.convertModelToDto(purchase),
                PurchaseDto.class);

        if(savedPurchase == null)
        {
            System.out.println("could not recieve respons from server");
            return;
        }


        if(savedPurchase.getId() == -1)
            System.out.println("Could not add purchase! Either the client or book does not exist!");
        else
            System.out.println("savedPurchase: " + savedPurchase);
    }





    // ------------------ MASTER MENU ------------------
    public void menu(){
        while(true)
        {
            System.out.println("1.Manage Books|2.Manage Clients|3.Buy Book|4.Sale History|5.Most Spent per Client|6.Most bought books|0.exit");
            int x = scanner.nextInt();
            switch(x) {
                case 1:
                    bookMenu();
                    break;
                case 2:
                    clientMenu();
                    break;
                case 3:
                    buyBook();
                    break;
                case 4:
                    showPurchases();
                    break;
                case 5:
                    //service.spentMost().stream().forEach(pair->System.out.println(pair.getKey().getName()+":"+pair.getValue()));
                    break;
                case 6:
                    //service.mostBought().stream().forEach(pair->System.out.println(pair.getKey().getTitle()+":"+pair.getValue()));
                    break;
                case 0:
                    scanner.close();
                    return;
                default:
                    System.out.println("Invalid input");
            }
        }
    }
}
