package ro.ubb.bookstore.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.web.client.RestTemplate;
import ro.ubb.bookstore.client.ui.InputOutput;

public class ClientApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro.ubb.bookstore.client.config"
                );

        RestTemplate restTemplate = context.getBean(RestTemplate.class);

        InputOutput UI = new InputOutput(restTemplate);
        UI.menu();
    }
}
